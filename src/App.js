import React, { Component } from 'react';
import Banner from './components/banner';
import MoviesList from './components/movielist';
import getMovie from './components/functions';

class Movie extends Component {
  constructor(props) {
    super(props)
    this.state = {
      moviesLoop: [],
      moviesFav: [],
      searchVal: ""
    }
  }

  componentWillMount() {
    getMovie('now_playing', (movies)=>{
      this.setState({ moviesLoop: movies.results })
      window.movies = movies.results
    })
  }


  filterMovies(event){
    if (event.target.value.length > 1){
      let updatedList = window.movies
      updatedList = updatedList.filter((movie) => {
        return movie.original_title.toLowerCase().search(event.target.value.toLowerCase()) !== -1;
      });
      this.setState({moviesLoop: updatedList})
    }else{
      this.setState({ moviesLoop: window.movies })
    }
  }

  searchMovies(event){
    this.setState({searchVal: event.target.value })
    if (this.state.searchVal.length > 1){

      getMovie('movie', (movies)=>{
        this.setState({ moviesLoop: movies.results })
      }, "/search/", "&query="+this.state.searchVal.replace(' ', '+'))

    }else if (this.state.searchVal.length <= 1 || this.state.searchVal === ""){
      this.setState({ moviesLoop: window.movies })
    }
  }

  onKeyDownFilter(event){
    if (event.keyCode === 8) {
     this.filterMovies(event)
    }
  }

  onKeyDownSearch(event){
    if (event.keyCode === 8) {
     this.searchMovies(event)
    }
  }

  checkFav(id){
    this.forceUpdate()
    for (var i = 0; i < this.state.moviesFav.length; i++) {
      if (this.state.moviesFav[i].id === id){
        return true
      }
    }
    return false
  }

  addFav(mId){
    let tempFav = this.state.moviesFav
    if (!this.checkFav(mId.id)) {
      tempFav.push(mId)
      this.setState({moviesFav: tempFav})
    }else{
      let filteredFav = this.state.moviesFav.filter(function(el) {
        return el.id !== mId.id;
      });

      this.setState({moviesFav: filteredFav})
    }
    return false;
  }

  render() {
    return  (
      <div>
        <Banner movieid={'now_playing'} />
        <div className="content">
          <section className="new-releases">
            <div className="section-heading">
              <h5 className="section-title">New Releases</h5>
              <form>
                <input onKeyDown={this.onKeyDownFilter.bind(this)} onChange={this.filterMovies.bind(this)} type="search" name="filter" placeholder="Filter..." />
                <input onKeyDown={this.onKeyDownSearch.bind(this)} onChange={this.searchMovies.bind(this)} type="search" name="search" placeholder="Search..." />
              </form>
            </div>
            <div id="movielist">
              <MoviesList list={this.state.moviesLoop} setfav={this.addFav.bind(this)} />
            </div>
            <div className="clear"></div>
          </section>
          <section className="favorites">
              <div className="section-heading">
                <h5 className="section-title">Favorites</h5>
              </div>
              <div id="favorites" className="movielist">
                <MoviesList list={this.state.moviesFav} setfav={this.addFav.bind(this)} />
              </div>
              <div className="clear"></div>
          </section>
          <div className="clear"></div>
        </div>
      </div>
    )
  }
}

export default Movie
