import React, { Component } from 'react';
import ReactDOM from 'react-dom';
import Movie from './App';

ReactDOM.render(
  <Movie />,
  document.getElementById('app')
);
