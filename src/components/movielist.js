import React, { Component } from 'react';
import MovieRow from './movie';

class MoviesList extends Component {

  render() {
    return (
      <div className="movielist">
          {
            this.props.list.map((movie) => {
              if (movie.backdrop_path) {
                return <MovieRow key={ movie.id }
                                    id={movie.id}
                                    picture={ movie.backdrop_path }
                                    title={ movie.title }
                                    runtime={ movie.runtime }
                                    released={ movie.release_date }
                                    rate={ movie.popularity }
                                    fav={ movie.vote_count }
                                    makefav={this.props.setfav}
                                    movie={movie}/>
              }
            })
          }
      </div>
    )
  }
}

export default MoviesList
