import React, { Component } from 'react';
import getMovie from './functions';

class Banner extends Component {
  constructor(props) {
    super(props)
    this.state = {
      id: "",
      image: "",
      title: "",
      released: "",
      runtime: "",
      rate: "",
      fav: ""
    }
  }

  componentWillMount() {
    getMovie(this.props.movieid, (movie)=>{
      if (this.props.movieid === 'now_playing') {
        movie = movie.results[0];
      }
      this.setState({
        id: movie.id,
        image: movie.backdrop_path,
        title: movie.title,
        released: movie.release_date,
        runtime: movie.runtime,
        rate: movie.popularity,
        fav: movie.vote_count
      })
    })
  }

  render() {
    return (
      <div className="banner" id={ this.state.id }>
        <div className="banner-title">
          <h2>{ this.state.title }</h2>
        </div>
        <div className="banner-container">
          <div className="banner_top" style={{ backgroundImage: 'url(http://image.tmdb.org/t/p/w1000' + this.state.image +')'}}></div>
          <div className="banner-info-wrapper">
            <div className="banner-info">
              <h4 className="banner-data">Released: <span className="banner-released">{ this.state.released }</span></h4>
              <h4 className="banner-data">Runtime: <span className="banner-runtime">{ this.state.runtime }</span></h4>
              <h4 className="banner-data">Rate: <span className="banner-rate">{ this.state.rate }%</span></h4>
              <h4 className="banner-data banner-fav"><span><i className="fa fa-heart-o"></i></span> <span className="banner-fav-text">{ this.state.fav }</span></h4>
            </div>
          </div>
        </div>
      </div>
    )
  }
}

export default Banner
