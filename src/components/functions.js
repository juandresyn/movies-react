const apiUrl = 'https://api.themoviedb.org/3';
const apiKey = '?api_key=4c171fe382a5d9550555530d1ae61021';

window['faved'] = [];
window['active'] = "";
window['movies'] = [];

const getMovie = (movieId, cb, endpoint = "/movie/", extra = "")=>{
  fetch(apiUrl + endpoint + movieId + apiKey + extra)
    .then((response) => {
      return response.json()
    })
    .then((movie) => {
      if (typeof cb === "function") {
        cb(movie)
      }
    })
}

export default getMovie
