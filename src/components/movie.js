import React, { Component } from 'react';
import getMovie from './functions';

class MovieRow extends Component {
  constructor(props) {
    super(props)
    this.state = {
      classNames: "movie-item movie-" + this.props.id + " "
    }
  }

  classHandler() {

    this.setState({classNames: " movie-item active"})
  }

  componentWillMount(){
    if (window.faved.indexOf(this.props.id) != -1) {
      this.setState({classNames: this.state.classNames + "faved "})
    }
  }

  onClick() {
    getMovie(this.props.id, (movie)=>{
      document.querySelector(".banner_top").style.backgroundImage = 'url(http://image.tmdb.org/t/p/w1000' + this.props.picture +')'
      document.querySelector(".banner-title h2").textContent = movie.title
      document.querySelector(".banner-released").textContent = movie.release_date
      document.querySelector(".banner-runtime").textContent = movie.runtime
      document.querySelector(".banner-rate").textContent = movie.popularity + "%"
      document.querySelector(".banner-fav-text").textContent = movie.vote_count
    })
  }

  favorite(mFav) {
    this.props.makefav(mFav)
    if (window.faved.indexOf(this.props.id) != -1) {
      window.faved = window.faved.filter(item => item !== this.props.id)
      this.setState({classNames: this.state.classNames.replace('faved ', '')})
      document.querySelector(".movie-" + this.props.id).classList.remove('faved')
    }else{
      window.faved.push(this.props.id)
      this.setState({classNames: this.state.classNames + "faved "})
    }
  }

  render() {
    return(
      <article id={this.props.id} onClick={this.onClick.bind(this)} className={this.state.classNames} style={{ backgroundImage: 'url(http://image.tmdb.org/t/p/w500' + this.props.picture + ')'}}>
        <div className="article-top">
          <span onClick={this.favorite.bind(this, this.props.movie)} className="movie-favs"><i className="fa fa-heart-o not-fav"></i><i className="fa fa-heart is-fav"></i> {this.props.fav}</span>
          <span className="movie-download"><i className="fa fa-download"></i></span>
        </div>
        <h6 className="movie-title">{this.props.title}</h6>
        <div className="blackout"></div>
      </article>
    )
  }
}

export default MovieRow
